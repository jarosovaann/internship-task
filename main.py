import os
import json
from datetime import datetime, timedelta
from typing import Tuple, Dict, List, Set, TextIO


def get_max_candidate(max_candidate: Tuple[int, int],
                      max_per_item_id: Dict[int, int], item_id: int)\
        -> Tuple[int, int]:
    if max_per_item_id.get(item_id) is None:
        max_per_item_id[item_id] = 1
    else:
        max_per_item_id[item_id] += 1
    if max_per_item_id[item_id] > max_candidate[1]:
        max_candidate = item_id, max_per_item_id[item_id]
    return max_candidate


def time_formater(time: str) -> datetime:
    if len(time) == 32:
        # usually the time format contains milliseconds but few does not
        time_form = datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%f%z")
    else:
        time_form = datetime.strptime(time, "%Y-%m-%dT%H:%M:%S%z")
    return time_form


def time_assigner(request_time: str, next_request_time: str,
                  time_data: List[timedelta]) -> None:
    request_time_form = time_formater(request_time)
    next_request_time_form = time_formater(next_request_time)
    time_data.append(next_request_time_form - request_time_form)


def data_analyzer(json_obj: TextIO, users_set: Set[int], item_set: Set[int],
                  time_data: List[timedelta], max_per_item_id: Dict[int, int],
                  max_candidate: Tuple[int, int]) -> Tuple[int, int]:
    content = json.load(json_obj)
    for user_id in content:
        users_set.add(user_id)  # 1. collecting unique users with set

        order_all = content.get(user_id)
        order_detail = list(order_all.keys())
        item_id = order_detail[1]

        item_set.add(item_id)
        # 2. collecting unique requests with set (same item_id count as one)

        datetime_request = order_all.get(order_detail[1])

        variant_returned = datetime_request[0][0][1]
        if variant_returned == 'similarInJsonList':
            max_candidate = get_max_candidate(max_candidate,
                                              max_per_item_id, item_id)
            # 5. keeping and updating current biggest number of request per
            # item_id stored in Tuple[item_id, current_biggest_number_of_request]
            """assumption: taking as relevant only datetime of request not datetime of next visit"""

        if len(datetime_request[0]) > 1:
            request_time = datetime_request[0][0][0]
            next_time = datetime_request[0][1][0]
            time_assigner(request_time, next_time, time_data)
            # 3. + 4.
            # time_assigner assigns to list every time difference so that average and median can be computed
            """assumption: collecting data from single record so if in other record there's same item_id
            it's counted as another data for average"""
    return max_candidate


def file_extracter() -> None:
    dir = 'data'
    users_set: Set[int] = set()
    item_set: Set[int] = set()
    time_data: List[timedelta] = list()
    max_per_item_id: Dict[int, int] = dict()
    max_rqst_candidate = 0, 0
    for file in os.scandir(dir):
        with open(file) as json_obj:
            max_rqst_candidate = data_analyzer(json_obj, users_set, item_set,
                                               time_data, max_per_item_id,
                                               max_rqst_candidate)

    time_data_len = len(time_data)
    average = sum(time_data, timedelta()) / time_data_len
    median = time_data[time_data_len//2]

    print(len(users_set))  # 1.
    print(len(item_set))  # 2.
    print(average)  # 3.
    print(median)  # 4.
    print(max_rqst_candidate)  # 5. Tuple[item_id, number_of_requests]


if __name__ == '__main__':
    file_extracter()
